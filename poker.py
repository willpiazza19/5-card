import random

class Card:
    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __str__(self):
        return f"{self.rank} of {self.suit}"

class Deck:
    def __init__(self):
        self.cards = [Card(rank, suit) for rank in self.ranks for suit in self.suits]
        random.shuffle(self.cards)

    def draw_card(self):
        return self.cards.pop()

class PokerHand:
    def __init__(self):
        self.hand = []

    def draw_hand(self, deck):
        for _ in range(5):
            self.hand.append(deck.draw_card())

    def show_hand(self):
        for card in self.hand:
            print(card)

    def evaluate_hand(self):
        pass  # Implement hand evaluation logic here

class PokerGame:
    def __init__(self):
        self.deck = Deck()
        self.player_hand = PokerHand()

    def start_game(self):
        print("Welcome to 5-Card Poker!\n")
        self.player_hand.draw_hand(self.deck)
        print("Your hand:")
        self.player_hand.show_hand()

    def draw_new_cards(self):
        while True:
            try:
                num_cards = int(input("How many cards do you want to discard and redraw? (0-3): "))
                if num_cards < 0 or num_cards > 3:
                    raise ValueError
                break
            except ValueError:
                print("Invalid input. Please enter a number between 0 and 3.")

        for _ in range(num_cards):
            # Remove discarded cards from the hand and draw new ones
            self.player_hand.hand.pop()
            self.player_hand.hand.append(self.deck.draw_card())

        print("\nYour new hand:")
        self.player_hand.show_hand()

    def end_game(self):
        print("\nThank you for playing!")

def main():
    game = PokerGame()
    game.start_game()
    game.draw_new_cards()
    game.end_game()

if __name__ == "__main__":
    main()
